
// Creato da carlo il 22/11/18
#include <iostream>
#include "matrici.h"

using namespace std;

int sistema_input(float mat[][MAXN], float b[]) {

  int n;

  do {
    cout << "Quante incognite? ";
    cin >> n;
  } while ( n < 1 || n > MAXN );

  sistema_input(mat, b, n);

  return n;

}

void sistema_input(float mat[][MAXN], float b[], int n) {
  matrice_input(mat, n);
  colonna_input(b, n);
}

void sostituisciColonna(const float mat[][MAXN], const float vett[], float ris[][MAXN], int n, int colonna) {

  for(int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      ris[i][j] = (j == colonna)? vett[i] : mat[i][j];
    }
  }

}

int risolvi_sistema(const float mat[][MAXN], const float b[], int n, float ris[]) {

  int res = 0;

  float det = determinante_matrice(mat, n);

  if(det == 0) // sistema non compatibile o con infinite soluzioni
    res = -1;
  else {
    float tmp[MAXN][MAXN];
    for(int i = 0; i < n; i++) {
      sostituisciColonna(mat, b, tmp, n, i);
      ris[i] = determinante_matrice(tmp, n) / det;
    }
  }

  return res;

}
