
// Creato da carlo il 22/11/18
#include <iostream>
#include "matrici.h"

using namespace std;

int main() {

  float mat[MAXN][MAXN];
  float b[MAXN], ris[MAXN];

  cout << "Inserisci la matrice dei coefficienti:" << endl;
  int n = matrice_input(mat);

  cout << "Inserisci la colonna dei termini noti:" << endl;
  colonna_input(b, n);

  cout << endl << "Ecco il sistema:" << endl;
  stampa_sistema(mat, b, n);

  if(risolvi_sistema(mat, b, n, ris) == 0) {
    cout << "La soluzione è: ";
    stampa_colonna(ris, n);
  } else {
    cout << "Il sistema ha infinite o nessuna soluzione" << endl;
  }

  return 0;
}
