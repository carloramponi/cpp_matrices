
// Creato da carlo il 22/11/18
#include "matrici.h"
#include <iostream>

using namespace std;

void sottoMatrice(const float mat[][MAXN], float mat2[][MAXN], int n, int riga, int colonna) {
  for(int i = 0; i < n-1; i++)
    for(int j = 0; j < n-1; j++)
      mat2[i][j] = mat[(i >= riga)? i+1 : i][(j >= colonna)? j+1 : j];
}

float determinante_matrice(const float mat[][MAXN], int n) {

  float det = 0;

  if(n == 1) {
    det = mat[0][0];
  } else if(n == 2) {
    det = mat[0][0]*mat[1][1] - mat[1][0]*mat[0][1];
  } else {
    float tmp[MAXN][MAXN];
    for(int i = 0; i < n; i++) {
      sottoMatrice(mat, tmp, n, 0, i);
      det += ((i % 2 == 0)? 1 : -1) * mat[0][i] * determinante_matrice(tmp, n-1);
    }
  }

  return det;
}
