
// Creato da carlo il 22/11/18
#include "matrici.h"
#include <iostream>
#include <iomanip>

using namespace std;

void stampa_matrice(const float mat[][MAXN], int n) {
  for(int i = 0; i < n; i++) {
    for(int j = 0; j < n; j++)
      cout << setw(2) << mat[i][j] << "  ";
    cout << endl;
  }
}


void stampa_colonna(const float b[], int n) {
  cout << "(";
  for(int i = 0; i < n; i++)
    cout << b[i] << ((i != n-1)? ", " : "");
  cout << ")" << endl;
}

void stampa_sistema(const float a[][MAXN], const float b[], int n) {
  for(int i = 0; i < n; i++) {
    if(i < n/2)
      cout << "⎧ ";
    else if(i == n/2)
      cout << "⎨ ";
    else
      cout << "⎩ ";
    for(int j = 0; j < n; j++)
      cout << setw(3) << a[i][j] << "(x"<< (j+1) << ")" << ((j != n-1)? " + " : " = ");
    cout << setw(3) << b[i] << endl;
  }
}
