#ifndef __MATRICI_H__
#define __MATRICI_H__

#define MAXN 10

int matrice_input(float mat[][MAXN]);
void matrice_input(float mat[][MAXN], int n);
void colonna_input(float b[], int n);
void stampa_colonna(const float b[], int n);
void stampa_matrice(const float mat[][MAXN], int n);
void stampa_sistema(const float a[][MAXN], const float b[], int n);
float determinante_matrice(const float mat[][MAXN], int n);
void sottoMatrice(const float mat[][MAXN], float mat2[][MAXN], int n, int riga, int colonna);
int sistema_input(float mat[][MAXN], float b[]);
void sistema_input(float mat[][MAXN], float b[], int n);
void sostituisciColonna(const float mat[][MAXN], const float vett[], float ris[][MAXN], int n, int colonna);
int risolvi_sistema(const float mat[][MAXN], const float b[], int n, float ris[]);

#endif
