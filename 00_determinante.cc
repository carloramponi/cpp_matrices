
// Creato da carlo il 22/11/18
#include <iostream>
#include "matrici.h"

using namespace std;

int main() {

  float mat[MAXN][MAXN];
  int n = matrice_input(mat);
  stampa_matrice(mat, n);

  float det = determinante_matrice(mat, n);

  cout << "Il determinante è: " << det << endl;

  return 0;
}
