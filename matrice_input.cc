
// Creato da carlo il 22/11/18
#include "matrici.h"
#include <iostream>

using namespace std;

void colonna_input(float b[], int n) {
  for(int i = 0; i < n; i++) {
    cout << "[" << i << "]:  ";
    cin >> b[i];
  }
}

int matrice_input(float mat[][MAXN]) {

  int n;

  do {
    cout << "Di che ordine?  ";
    cin >> n;
  } while(n < 1 || n > MAXN);

  matrice_input(mat, n);

  return n;

}

void matrice_input(float mat[][MAXN], int n) {

  for(int i = 0; i < n; i++) {
    for(int j = 0; j < n; j++) {
      cout << "[" << i << "][" << j << "]:  ";
      cin >> mat[i][j];
    }
  }

}
